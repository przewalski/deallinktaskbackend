<?php

use Illuminate\Database\Seeder;
use App\Models\Tender;

class TendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tender::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 4000; $i++) {
            Tender::create([
                'title' => $faker->sentence,
                'body' => $faker->paragraph,
            ]);
        }
    }
}
