<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTenderRequest;
use App\Models\Tender;

/**
 * Class TenderController
 * @package App\Http\Controllers
 */
class TenderController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        $tenders = Tender::orderBy('created_at', 'asc')->paginate(20);

        return response()->json($tenders);
    }

    /**
     * @param  CreateTenderRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CreateTenderRequest $request)
    {
        $tender = Tender::create($request->all());

        return response()->json($tender, 201);
    }

    /**
     * @param  CreateTenderRequest  $request
     * @param  Tender  $tender
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CreateTenderRequest $request, $id)
    {
        $tender = Tender::findOrFail($id)->update($request->all());

        return response()->json($tender, 200);
    }

    /**
     * @param  Tender  $tender
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        Tender::findOrFail($id)->delete();

        return response()->json('message', 204);
    }
}
