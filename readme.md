# Build Instructions:

* Clone the source repository from Bitbucket
    * On the command line, enter:
        * git clone https://przevalski@bitbucket.org/przewalski/deallinktaskbackend.git

* Create DATABASE

* Set the **.env**, write your data to:
     * DB_CONNECTION=
     * DB_HOST=
     * DB_PORT=
     * DB_DATABASE=
     * DB_USERNAME=
     * DB_PASSWORD=
       
* Launch the application on localhost and add domain to .env, to APP_DOMAIN variable

* On the command line, enter:
    1. **composer install**
    2. **php artisan migrate**
    3. **php artisan db:seed**
    