<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/tenders', 'TenderController@all');

Route::post('/tender/add', 'TenderController@create');

Route::post('/tender/edit/{id}', 'TenderController@update');

Route::get('/tender/delete/{id}', 'TenderController@delete');
